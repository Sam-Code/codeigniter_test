CREATE DATABASE  IF NOT EXISTS `BDCITEST` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;
USE `BDCITEST`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: BDCITEST
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Log`
--

DROP TABLE IF EXISTS `Log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Log` (
  `LogId` bigint(20) NOT NULL AUTO_INCREMENT,
  `Controlador` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `Accion` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `MetodoHttp` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `FechaHoraEvento` datetime DEFAULT NULL,
  PRIMARY KEY (`LogId`)
) ENGINE=InnoDB AUTO_INCREMENT=233 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Log`
--

LOCK TABLES `Log` WRITE;
/*!40000 ALTER TABLE `Log` DISABLE KEYS */;
INSERT INTO `Log` VALUES (118,'key','index','put','2016-04-20 11:16:20'),(119,'servicios','index','get','2016-04-20 11:18:28'),(120,'usuarios','index','get','2016-04-20 11:18:31'),(121,'servicios','index','get','2016-04-20 11:18:35'),(122,'usuarios','index','get','2016-04-20 11:19:21'),(123,'usuarios','crear','get','2016-04-20 11:19:23'),(124,'usuarios','crear','post','2016-04-20 11:19:27'),(125,'usuarios','index','get','2016-04-20 11:19:27'),(126,'servicios','index','get','2016-04-20 11:21:59'),(127,'key','index','put','2016-04-20 11:22:00'),(128,'servicios','index','get','2016-04-20 11:22:30'),(129,'key','index','get','2016-04-20 11:22:33'),(130,'servicios','index','get','2016-04-20 11:22:57'),(131,'servicios','index','get','2016-04-20 11:23:48'),(132,'servicios','index','get','2016-04-20 11:25:36'),(133,'servicios','index','get','2016-04-20 11:26:27'),(134,'servicios','index','get','2016-04-20 11:26:28'),(135,'servicios','index','get','2016-04-20 11:26:37'),(136,'servicios','index','get','2016-04-20 11:26:59'),(137,'servicios','index','get','2016-04-20 11:27:23'),(138,'servicios','index','get','2016-04-20 11:27:40'),(139,'servicios','index','get','2016-04-20 11:27:50'),(140,'servicios','index','get','2016-04-20 11:28:26'),(141,'key','index','put','2016-04-20 11:28:28'),(142,'api','usuarios','get','2016-04-20 11:28:41'),(143,'api','usuarios','get','2016-04-20 11:28:52'),(144,'api','usuarios','get','2016-04-20 11:29:17'),(145,'servicios','index','get','2016-04-20 11:30:07'),(146,'key','index','put','2016-04-20 11:30:08'),(147,'api','usuarios','get','2016-04-20 11:30:09'),(148,'key','index','put','2016-04-20 11:30:14'),(149,'api','usuarios','get','2016-04-20 11:30:15'),(150,'servicios','index','get','2016-04-20 11:30:36'),(151,'servicios','index','get','2016-04-20 11:32:00'),(152,'key','index','put','2016-04-20 11:32:02'),(153,'api','usuarios','get','2016-04-20 11:32:03'),(154,'servicios','index','get','2016-04-20 11:32:17'),(155,'key','index','put','2016-04-20 11:32:19'),(156,'api','usuarios','get','2016-04-20 11:32:20'),(157,'key','index','put','2016-04-20 11:32:27'),(158,'api','usuarios','get','2016-04-20 11:32:27'),(159,'servicios','index','get','2016-04-20 11:32:49'),(160,'key','index','put','2016-04-20 11:32:50'),(161,'api','usuarios','get','2016-04-20 11:32:51'),(162,'servicios','index','get','2016-04-20 11:33:08'),(163,'key','index','put','2016-04-20 11:33:10'),(164,'api','usuarios','get','2016-04-20 11:33:11'),(165,'servicios','index','get','2016-04-20 11:34:08'),(166,'servicios','index','get','2016-04-20 11:34:31'),(167,'key','index','put','2016-04-20 11:34:32'),(168,'api','usuarios','get','2016-04-20 11:34:33'),(169,'key','index','get','2016-04-20 11:34:38'),(170,'api','usuarios','get','2016-04-20 11:34:39'),(171,'key','index','get','2016-04-20 11:34:41'),(172,'key','index','get','2016-04-20 11:34:45'),(173,'api','usuarios','get','2016-04-20 11:34:46'),(174,'servicios','index','get','2016-04-20 11:35:13'),(175,'servicios','index','get','2016-04-20 11:38:32'),(176,'key','index','put','2016-04-20 11:38:33'),(177,'api','logs','get','2016-04-20 11:38:34'),(178,'servicios','index','get','2016-04-20 11:38:52'),(179,'servicios','index','get','2016-04-20 11:38:59'),(180,'servicios','index','get','2016-04-20 11:39:06'),(181,'key','index','put','2016-04-20 11:39:10'),(182,'api','usuarios','get','2016-04-20 11:39:12'),(183,'api','logs','get','2016-04-20 11:39:14'),(184,'servicios','index','get','2016-04-20 11:39:42'),(185,'key','index','put','2016-04-20 11:39:43'),(186,'api','usuarios','get','2016-04-20 11:39:44'),(187,'api','logs','get','2016-04-20 11:39:45'),(188,'api','usuarios','get','2016-04-20 11:39:47'),(189,'api','logs','get','2016-04-20 11:39:48'),(190,'usuarios','index','get','2016-04-20 11:39:52'),(191,'servicios','index','get','2016-04-20 11:39:54'),(192,'servicios','index','get','2016-04-20 11:40:42'),(193,'key','index','put','2016-04-20 11:40:45'),(194,'servicios','index','get','2016-04-20 11:41:00'),(195,'key','index','put','2016-04-20 11:41:02'),(196,'servicios','index','get','2016-04-20 11:41:11'),(197,'key','index','put','2016-04-20 11:41:12'),(198,'key','index','put','2016-04-20 11:41:16'),(199,'servicios','index','get','2016-04-20 11:41:59'),(200,'key','index','put','2016-04-20 11:42:00'),(201,'key','index','put','2016-04-20 11:42:01'),(202,'servicios','index','get','2016-04-20 11:42:17'),(203,'key','index','put','2016-04-20 11:42:18'),(204,'servicios','index','get','2016-04-20 11:43:23'),(205,'key','index','put','2016-04-20 11:43:24'),(206,'servicios','index','get','2016-04-20 11:44:21'),(207,'key','index','put','2016-04-20 11:44:22'),(208,'servicios','index','get','2016-04-20 11:44:34'),(209,'key','index','put','2016-04-20 11:44:35'),(210,'servicios','index','get','2016-04-20 11:46:06'),(211,'key','index','put','2016-04-20 11:46:07'),(212,'servicios','index','get','2016-04-20 11:46:23'),(213,'key','index','put','2016-04-20 11:46:24'),(214,'servicios','index','get','2016-04-20 11:47:52'),(215,'key','index','put','2016-04-20 11:47:55'),(216,'api','usuarios','get','2016-04-20 11:47:58'),(217,'api','logs','get','2016-04-20 11:47:59'),(218,'key','index','put','2016-04-20 11:48:01'),(219,'key','index','put','2016-04-20 11:48:02'),(220,'usuarios','index','get','2016-04-20 11:48:22'),(221,'usuarios','index','get','2016-04-20 11:48:39'),(222,'usuarios','index','get','2016-04-20 11:48:41'),(223,'usuarios','index','get','2016-04-20 11:48:42'),(224,'servicios','index','get','2016-04-20 11:48:44'),(225,'key','index','put','2016-04-20 11:48:46'),(226,'api','usuarios','get','2016-04-20 11:48:47'),(227,'api','logs','get','2016-04-20 11:48:48'),(228,'servicios','index','get','2016-04-20 11:49:57'),(229,'servicios','index','get','2016-04-20 11:49:59'),(230,'key','index','put','2016-04-20 11:50:00'),(231,'api','usuarios','get','2016-04-20 11:50:01'),(232,'api','logs','get','2016-04-20 11:50:02');
/*!40000 ALTER TABLE `Log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Usuario`
--

DROP TABLE IF EXISTS `Usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Usuario` (
  `UsuarioId` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ApellidoPaterno` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `ApellidoMaterno` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `FechaCreacion` datetime DEFAULT NULL,
  PRIMARY KEY (`UsuarioId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Usuario`
--

LOCK TABLES `Usuario` WRITE;
/*!40000 ALTER TABLE `Usuario` DISABLE KEYS */;
INSERT INTO `Usuario` VALUES (1,'Sam','Hernandez','Calderon','2016-04-19 07:11:00'),(2,'a','a','a','2016-04-20 11:19:27');
/*!40000 ALTER TABLE `Usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `keys`
--

DROP TABLE IF EXISTS `keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `keys`
--

LOCK TABLES `keys` WRITE;
/*!40000 ALTER TABLE `keys` DISABLE KEYS */;
INSERT INTO `keys` VALUES (4,'123',10,0,0),(55,'wsskcs0k4okookckg40w8kc440c0wgss8ssk4kwg',1,1,1461171000);
/*!40000 ALTER TABLE `keys` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-20 11:51:13
