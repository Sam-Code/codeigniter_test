<html>
	<head>
		<title><?= $title ?></title>
		<link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap.css"); ?>" />
		<link rel="stylesheet" href="<?php echo base_url("assets/css/sticky-footer.css"); ?>" />       
    <script rel="text/javascript" src="<?= base_url('assets/js/jquery-1.12.3.min.js'); ?>" ></script>
    <script rel="text/javascript" src="<?= base_url('assets/js/angular.min.js'); ?>" ></script>
	</head>
	<style>
		.footer {
    position: absolute;
    bottom: 0;
    width: 100%;
    height: 60px;
    background-color: #f5f5f5;
}
	</style>
	<body>    
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">CodeIgniter TEST</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="<?= site_url('usuarios'); ?>">Usuarios</a></li>
            <li><a href="<?= site_url('logs'); ?>">Logs</a></li>
            <li><a href="<?= site_url('servicios'); ?>">Servicios</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
	<div class="container">