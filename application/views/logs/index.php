<h1 style="margin-top:100px;"><?= $title; ?></h1>
<hr/>
<div class="clearfix"></div>
<table class="table table-striped table-bordered table-hover table-condensed"> 
	<thead>
		<th>Controlador</th>
		<th>Acción</th>
		<th>Fecha Hora</th>
	</thead>
	<tbody>
		<?php foreach ($events as $event): ?>
			<tr>			
				<td><?= $event->Controlador; ?></td>
				<td><?= $event->Accion; ?></td>
				<td><?= $event->FechaHoraEvento; ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>