<h1 style="margin-top:100px;"><?= $title; ?></h1>
<hr/>
<?= $message ?>
<div style="padding-bottom: 45px">
	<a href="<?= site_url('usuarios/crear') ?>" class="btn btn-success pull-right">Nuevo Usuario</a>
</div>	
<div class="clearfix"></div>


<table class="table table-striped table-bordered table-hover table-condensed"> 
	<thead>
		<th>Nombre</th>
		<th>Apellido Paterno</th>
		<th>Apellido Materno</th>
		<!--<th>Acciones</th>-->
	</thead>
	<tbody>
		<?php foreach ($usuarios as $usuario): ?>
			<tr>			
				<td><?= $usuario->Nombre; ?></td>
				<td><?= $usuario->ApellidoPaterno; ?></td>
				<td><?= $usuario->ApellidoMaterno; ?></td>
				<!--<td> 
					<a class="btn btn-default" href="<?= site_url('usuario/'.$usuario->UsuarioId); ?>" role="button">
						<span></span>
					</a> 
				</td>-->
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>