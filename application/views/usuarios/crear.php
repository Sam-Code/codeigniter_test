<h1 style="margin-top:100px;"><?= $title; ?></h1>
<hr/>
<?php echo validation_errors(); ?>

<?php echo form_open('usuarios/crear'); ?>

    <div class="form-group">
		<label for="Nombre">Nombre:</label>
		<input type="text" class="form-control" id="Nombre" name="Nombre" placeholder="Nombre">
	</div>
	<div class="form-group">
		<label for="ApellidoPaterno">ApellidoPaterno:</label>
		<input type="text" class="form-control" id="ApellidoPaterno" name="ApellidoPaterno" placeholder="ApellidoPaterno">
	</div>
	<div class="form-group">
		<label for="ApellidoMaterno">ApellidoMaterno:</label>
		<input type="text" class="form-control" id="ApellidoMaterno" name="ApellidoMaterno" placeholder="ApellidoMaterno">
	</div>	
	<button type="submit" name="submit" class="btn btn-success pull-right">Crear</button>

</form>