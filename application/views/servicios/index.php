<h1 style="margin-top:100px;"><?= $title; ?></h1>
<hr/>
<div class="clearfix"></div>

<div ng-app="myApp" ng-controller="myCtrl" > 
	
<div>	
	<div class="form-group">
		<label for="apikey">API-KEY:</label>
		<input type="text" class="form-control input-sm" id="apikey" name="apikey" value="{{apiKey}}" readonly>			
	</div>
	<div class="form-group">
      	<button class="btn btn-primary btn-sm" ng-click="getApiKey()">Generar API-KEY</button>
  	</div>	
</div>
<div ng-bind-html="message">
</div>


<div class="panel panel-default">  
  <div class="panel-body">
  	<button class="btn btn-success btn-sm" ng-click="getUsuarios()">Ver Usuarios</button>
	<button class="btn btn-success btn-sm" ng-click="getLogs()">Ver Logs</button>    
  </div>
</div>

<div ng-show="verUsuarios">
	<h3>Usuarios</h3>
	<table class="table table-striped table-bordered table-hover table-condensed"> 
		<thead>
			<th>Nombre</th>
			<th>Apellido Paterno</th>
			<th>Apellido Materno</th>		
		</thead>
		<tbody>
			<tr ng-repeat="usuario in usuarios">
				<td>{{usuario.Nombre}}</td>
				<td>{{usuario.ApellidoPaterno}}</td>
				<td>{{usuario.ApellidoMaterno}}</td>
			</tr>		
		</tbody>
	</table>
</div>


<div ng-show="verLogs">
	<h3>Logs</h3>
	<table class="table table-striped table-bordered table-hover table-condensed"> 
		<thead>
			<th>Fecha Hora</th>
			<th>Controlador</th>
			<th>Accion</th>		
			<th>MetodoHttp</th>		
		</thead>
		<tbody>
			<tr ng-repeat="evento in eventos">
				<td>{{evento.FechaHoraEvento}}</td>
				<td>{{evento.Controlador}}</td>
				<td>{{evento.Accion}}</td>
				<td>{{evento.MetodoHttp}}</td>
			</tr>		
		</tbody>
	</table>
</div>






<script type="text/javascript">

		var app = angular.module('myApp', []);

		app.controller('myCtrl', ['$scope','$http','$sce','$location', function($scope, $http,$sce,$location) {

			$scope.verUsuarios = false;
			$scope.verLogs = false;

			var url = window.location.href.replace("servicios", "");

			$scope.getApiKey = function () {

				var vServiceUrl = url + 'key';

				$http({
				    method: 'PUT',
				    url: vServiceUrl,
			     	headers: {
			        	'X-API-KEY': '123'
				    }
				}).then(function(response) {
					$scope.apiKey = response.data.key;										        				
			    });
								
			};

			$scope.getUsuarios = function () {

				var vServiceUrl = url + 'api/usuarios';

				if(angular.element('#apikey').val() == ''){
					$scope.message = $sce.trustAsHtml('<div class="alert alert-danger alert-sm" role="alert"> Primero genera un apikey. </div>');					
				}else{

					$scope.message = $sce.trustAsHtml('');					

					$http({
					    method: 'Get',
					    url: vServiceUrl,
				     	headers: {
				        	'X-API-KEY': angular.element('#apikey').val()
					    }
					}).then(function(response) {
						$scope.usuarios = response.data;							        
						$scope.verUsuarios = true;
						$scope.verLogs = false;
				    });
				}
								
			};

			$scope.getLogs = function () {

				var vServiceUrl = url + 'api/logs';

				if(angular.element('#apikey').val() == ''){
					$scope.message = $sce.trustAsHtml('<div class="alert alert-danger" role="alert"> Primero genera un apikey. </div>');					
				}else{

					$scope.message = $sce.trustAsHtml('');

					$http({
					    method: 'Get',
					    url: vServiceUrl,
				     	headers: {
				        	'X-API-KEY': angular.element('#apikey').val()
					    }
					}).then(function(response) {
						$scope.eventos = response.data;							        
						$scope.verLogs = true;
						$scope.verUsuarios = false;
				    });
				}
								
			};
						  
		}]);
		
			
		
	</script>

</div>

