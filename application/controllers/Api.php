<?php
require(APPPATH.'libraries/SAM_Controller.php');

class Api extends SAM_Controller{

	function __construct(){
    	parent::__construct();  
  	}

	public function usuarios_get(){

		$this->load->model('Usuario');
        $users = $this->Usuario->getAll();

		$this->response($users);

	}

	public function logs_get(){

		$this->load->model('Log');
        $eventos = $this->Log->getAll();

		$this->response($eventos);

	}



}
