<?php

class Usuarios extends CI_Controller{

	function __construct(){
    	parent::__construct();  
    	$this->load->model('Log'); 
    	$this->load->model('Usuario'); 
    	$this->load->helper('url_helper');
    	$this->load->library('session');

    	$this->Log->save();
  	}

	public function index(){

        $data['usuarios'] = $this->Usuario->getAll();
        $data['title'] = 'Usuarios';

        $_session_data = $this->session->flashdata('message');
		$data['message'] = $_session_data['message'];

        $this->load->view('templates/header', $data);
        $this->load->view('usuarios/index', $data);
        $this->load->view('templates/footer');
    }


    public function crear(){

    	$this->load->helper('form');
	    $this->load->library('form_validation');

	    $data['title'] = 'Crear usuario';

	    $this->form_validation->set_rules('Nombre', 'Nombre', 'required');	    
	    $this->form_validation->set_rules('ApellidoPaterno', 'ApellidoPaterno', 'required');	    
	    $this->form_validation->set_rules('ApellidoMaterno', 'ApellidoMaterno', 'required');

	    if ($this->form_validation->run() === FALSE)
	    {
	        $this->load->view('templates/header', $data);
	        $this->load->view('usuarios/crear');
	        $this->load->view('templates/footer');

	    }
	    else
	    {
	        $this->Usuario->create();
	        $data['message'] = '<div class="alert alert-success" role="alert"> Usuario creado correctamente. </div>';			
			$this->session->set_flashdata('message',$data);
			redirect('usuarios/index');
	    }


        
    }



}
