<?php

class Servicios extends CI_Controller{

	function __construct(){
    	parent::__construct();      	
    	$this->load->model('Log'); 
    	$this->load->helper('url_helper');
    	$this->load->library('session');

    	$this->Log->save();
  	}

	public function index(){
        
        $data['title'] = 'Servicios';

        $this->load->view('templates/header', $data);
        $this->load->view('servicios/index', $data);
        $this->load->view('templates/footer');
    }


}
