<?php

class Logs extends CI_Controller{

	function __construct(){
    	parent::__construct();  
    	$this->load->model('Log'); 
    	$this->load->helper('url_helper');
  	}

	public function index(){

        $data['events'] = $this->Log->getAll();
        $data['title'] = 'Log';
		
        $this->load->view('templates/header', $data);
        $this->load->view('logs/index', $data);
        $this->load->view('templates/footer');
    }



}
