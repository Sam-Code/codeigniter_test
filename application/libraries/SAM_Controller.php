<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'libraries/REST_Controller.php');

/**
 * Para anexar el log de acciones y quiza algunas otras funcionalidades
 */
abstract class SAM_Controller extends REST_Controller {
    
    public function __construct($config = 'rest')
    {
        parent::__construct();


        $this->load->model('Log');
        $this->Log->save();
    }


   

}
