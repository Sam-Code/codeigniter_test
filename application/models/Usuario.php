<?php
 
class Usuario extends CI_Model {
  
  private $table = 'Usuario';
  
  function __construct(){
    parent::__construct();    
  }

  function getAll(){

    $this->db->order_by('UsuarioId', 'DESC');
    $query = $this->db->get($this->table, 0, 10);
     
    return $query->result();
  }

	public function create(){

	    $data = array(
	        'Nombre' => $this->input->post('Nombre'),
	        'ApellidoPaterno' => $this->input->post('ApellidoPaterno'),
	        'ApellidoMaterno' => $this->input->post('ApellidoMaterno'),
	        'FechaCreacion' => date("Y-m-d H:i:s")
	    );

	    return $this->db->insert($this->table, $data);
	}


}