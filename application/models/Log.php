<?php
 
class Log extends CI_Model {
  
  private $table = 'Log';
  
  function __construct(){
    parent::__construct();
  }

  public function getAll(){
  	
  	$this->db->order_by('FechaHoraEvento', 'DESC');
    $query = $this->db->get($this->table);
     
    return $query->result();

  }

  public function save(){
  	
  	$data = [
		"Controlador" => $this->router->fetch_class(),
	 	"Accion" => $this->router->fetch_method(),
    "MetodoHttp" => $this->input->method(),
		"FechaHoraEvento" => date("Y-m-d H:i:s") ];
		
	$this->db->insert($this->table,$data);

  }


}